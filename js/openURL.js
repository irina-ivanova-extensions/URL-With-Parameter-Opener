function setErrorText(errorText) {
	var divError=document.getElementById("error");
	divError.innerText=errorText + "\n";
};

function returnError(errorText) {
	setErrorText(errorText);
	throw "";
};

var parameter,
	url,
	isError=0;

function removeSpaces(string) {
	while (string.charAt(string.length - 1) === " ") {
		string=string.slice(0, string.length - 1);
	}

	if (string.charAt(0) === " ") {
		var temp=string.split(" ");
		string=temp[temp.length - 1];
	}

	return string;
};

function removeSkypeFormatting(string) {
	if (string.charAt(0) === "[") {
		var temp=string.split(" ");
		string=temp[temp.length - 1];
	}

	return string;
};

function openWindow() {
	window.open(url + parameter);
};

var list=[];

$(function () {
	chrome.storage.sync.get(function (items) {
		if (items.savedHistory0 === undefined) {
    	list[0]="";
    } else {
    	list[0]=items.savedHistory0;
    }

    if (items.savedHistory1 === undefined) {
    	list[1]="";
    } else {
    	list[1]=items.savedHistory1;
    }

    if (items.savedHistory2 === undefined) {
    	list[2]="";
    } else {
      list[2]=items.savedHistory2;
    }

    if (items.savedHistory3 === undefined) {
      list[3]="";
    } else {
      list[3]=items.savedHistory3;
    }

    if (items.savedHistory4 === undefined) {
    	list[4]="";
    } else {
      list[4]=items.savedHistory4;
    }
	});

  $("#parameter").autocomplete({
    source: function(request, response) {
    	var results=$.ui.autocomplete.filter(list, request.term);
      if (list[0] === "") {
    		response(results.slice(0, 0));
      } else if (list[1] === "") {
      	response(results.slice(0, 1));
      } else if (list[2] === "") {
        response(results.slice(0, 2));
      } else if (list[3] === "") {
        response(results.slice(0, 3));
      } else if (list[4] === "") {
        response(results.slice(0, 4));
      } else {
      	response(results.slice(0, 5));
      }
    }
  });
});

function saveHistory(parameter) {
	chrome.storage.sync.get(function (items) {
		list[0]=items.savedHistory0;
    list[1]=items.savedHistory1;
    list[2]=items.savedHistory2;
    list[3]=items.savedHistory3;
    list[4]=items.savedHistory4;

    for (var i=4; i > 0; i--) {
    	list[i]=list[i-1];
    }
    list[0]=parameter;

    chrome.storage.sync.set({
    	savedHistory0: list[0],
    	savedHistory1: list[1],
    	savedHistory2: list[2],
    	savedHistory3: list[3],
    	savedHistory4: list[4]
    }, function() {});
  });
}

function openURL() {
	chrome.storage.sync.get(function (item) {
		url=item.savedUrl;

		parameter=document.getElementById("parameter").value;
		parameter=removeSpaces(parameter);
		parameter=removeSkypeFormatting(parameter);

		if (parameter === "") {
			isError=1;
			returnError("Please insert parameter");
    } else if (url === undefined) {
      isError=1;
			returnError("Please define URL in Options");
		} else {
			saveHistory(parameter);
			openWindow();
		}
	});
};

function inputParameterListener(e) {
    var enter=13;

	if (e.keyCode === enter) {
		openURL();
	}
};

function listenInputParameter(inputParameter) {
	if (inputParameter.addEventListener) {
		inputParameter.addEventListener("keydown", inputParameterListener, false);
	} else if (inputParameter.attachEvent) {
		inputParameter.attachEvent("keydown", inputParameterListener);
	}
};

function listenParameter() {
	listenInputParameter(document.getElementById("parameter"));
};

if (window.addEventListener) {
	window.addEventListener("load", this.listenParameter, false);
} else if (window.attachEvent) {
	window.attachEvent("onload", this.listenParameter);
} else {
	document.addEventListener("load", this.listenParameter, false);
}
